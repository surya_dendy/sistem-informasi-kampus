@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::mahasiswa.index') }}"></x-backlink>

    <x-panel title="Detil Mahasiswa">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $mahasiswa->id }}</td></tr>
        <tr><td>Nama</td><td>{{ $mahasiswa->nama }}</td></tr>
        <tr><td>Nim</td><td>{{ $mahasiswa->nim }}</td></tr>
        <tr><td>Gender</td><td>{{ $mahasiswa->gender }}</td></tr>
        <tr><td>Tmpt Lahir</td><td>{{ $mahasiswa->tmpt_lahir }}</td></tr>
        <tr><td>Tgl Lahir</td><td>{{ $mahasiswa->tgl_lahir }}</td></tr>
        <tr><td>Created At</td><td>{{ $mahasiswa->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $mahasiswa->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
