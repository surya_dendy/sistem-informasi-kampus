{!! form()->text('nama')->label('Nama Panjang') !!}
{!! form()->text('nim')->label('Nim')->attribute('maxlength',8) !!}
{!! form()->dropdown('gender',['laki-laki'=>'Laki Laki','perempuan'=>'Perempuan'])->placeholder('Masukkan
Gender')->label('Gender') !!}
{!! form()->text('tmpt_lahir')->label('Tempat Lahir') !!}
{!! form()->datepicker('tgl_lahir',null,'d M Y')->label('Tanggal Lahir') !!}
{!! form()->action([
form()->submit('Simpan Data'),
form()->link('Batal', route('modules::mahasiswa.index'))
]) !!}