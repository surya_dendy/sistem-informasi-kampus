<?php

namespace Modules\Mahasiswa\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Matakuliah\Models\Matakuliah;

class Mahasiswa extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'mahasiswa';

    protected $guarded = [];

    protected $searchableColumns = ["nama", "nim", "gender", "tmpt_lahir", "tgl_lahir",];

    protected $dates = ['tgl_lahir'];

    public function matakuliah()
    {
        return $this->belongsToMany(Matakuliah::class);
    }
}
