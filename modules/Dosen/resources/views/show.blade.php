@extends('laravolt::layouts.app')

@section('content')

<x-backlink url="{{ route('modules::dosen.index') }}"></x-backlink>

<x-panel title="Detil Dosen">
    <table class="ui table definition">
        <tr>
            <td>Id</td>
            <td>{{ $dosen->id }}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{{ $dosen->nama }}</td>
        </tr>
        <tr>
            <td>Nip</td>
            <td>{{ $dosen->nip }}</td>
        </tr>
        <tr>
            <td>Gelar</td>
            <td>{{ $dosen->gelar }}</td>
        </tr>
        <tr>
            <td>Strata</td>
            <td>{{ $riwayat_pendidikan->strata }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td>{{ $riwayat_pendidikan->jurusan }}</td>
        </tr>
        <tr>
            <td>Nama Perguruan Tinggi</td>
            <td>{{ $riwayat_pendidikan->sekolah }}</td>
        </tr>
        <tr>
            <td>Tahun Mulai</td>
            <td>{{ $riwayat_pendidikan->tahun_mulai }}</td>
        </tr>
        <tr>
            <td>Tahun Selesai</td>
            <td>{{ $riwayat_pendidikan->tahun_selesai }}</td>
        </tr>
        <tr>
            <td>Created At</td>
            <td>{{ $dosen->created_at }}</td>
        </tr>
        <tr>
            <td>Updated At</td>
            <td>{{ $dosen->updated_at }}</td>
        </tr>
    </table>
</x-panel>

@stop