@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::dosen.index') }}"></x-backlink>

    <x-panel title="Tambah Dosen">
        {!! form()->post(route('modules::dosen.store'))->horizontal()->multipart() !!}
        @include('dosen::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
