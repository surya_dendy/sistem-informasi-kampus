@extends('laravolt::layouts.app')

@section('content')

<x-backlink url="{{ route('modules::dosen.index') }}"></x-backlink>

<x-panel title="Edit Dosen">
    {!! form()->bind($dosen)->put(route('modules::dosen.update',
    $dosen->getKey()))->horizontal()->multipart() !!}
    {!! form()->text('nama')->label('Nama') !!}
    {!! form()->text('nip')->label('Nip') !!}
    {!! form()->text('gelar')->label('Gelar') !!}
    {!! form()->dropdown('strata',$strata,$riwayat_pendidikan->strata)->placeholder('Masukkan Strata
    Pendidikan')->label('Strata') !!}
    {!! form()->text('jurusan',$riwayat_pendidikan->jurusan)->label('Jurusan') !!}
    {!! form()->text('sekolah',$riwayat_pendidikan->sekolah)->label('Nama Perguruan Tinggi') !!}
    {!! form()->datepicker('tahun_mulai',$riwayat_pendidikan->tahun_mulai,'d M Y')->label('Tahun Masuk') !!}
    {!! form()->datepicker('tahun_selesai',$riwayat_pendidikan->tahun_selesai, 'd M Y')->label('Tahun Selesai') !!}
    {!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('modules::dosen.index'))
    ]) !!}
    {!! form()->close() !!}
</x-panel>

@stop