{!! form()->text('nama')->label('Nama') !!}
{!! form()->text('nip')->label('Nip') !!}
{!! form()->text('gelar')->label('Gelar') !!}
{!! form()->dropdown('strata',$strata)->placeholder('Masukkan Strata Pendidikan')->label('Strata') !!}
{!! form()->text('jurusan')->label('Jurusan') !!}
{!! form()->text('sekolah')->label('Nama Perguruan Tinggi') !!}
{!! form()->datepicker('tahun_mulai',null,'d M Y')->label('Tahun Masuk') !!}
{!! form()->datepicker('tahun_selesai',null, 'd M Y')->label('Tahun Selesai') !!}
{!! form()->action([
form()->submit('Simpan'),
form()->link('Batal', route('modules::dosen.index'))
]) !!}