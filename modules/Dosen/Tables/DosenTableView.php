<?php

namespace Modules\Dosen\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Dosen\Models\Dosen;

class DosenTableView extends TableView
{
    public function source()
    {
        return Dosen::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nama')->sortable(),
            Text::make('nip')->sortable(),
            Text::make('gelar')->sortable(),
            Raw::make(function ($item) {
                return "<a href='" . route('modules::dosen.show', $item->id) . "'><i class='eye icon'></a>";
            }, 'Riwayat Pendidikan'),
            RestfulButton::make('modules::dosen')->except('view'),
        ];
    }
}
