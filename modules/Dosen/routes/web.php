<?php

use Modules\Dosen\Controllers\DosenController;

$router->group(
    [
        'prefix' => config('modules.dosen.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.dosen.route.middleware'),
    ],
    function ($router) {
        $router->resource('dosen', DosenController::class);
    }
);
