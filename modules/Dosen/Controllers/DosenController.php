<?php

namespace Modules\Dosen\Controllers;

use App\Models\RiwayatPendidikan;
use Illuminate\Routing\Controller;
use Modules\Dosen\Requests\Store;
use Modules\Dosen\Requests\Update;
use Modules\Dosen\Models\Dosen;
use Modules\Dosen\Tables\DosenTableView;

class DosenController extends Controller
{
    public function index()
    {
        return DosenTableView::make()->view('dosen::index');
    }

    public function create()
    {
        $strata = ['Sarjana (S1)' => 'Sarjana (S1)', 'Magister (S2)' => 'Magister (S2)', 'Doktor (S3)' => 'Doktor (S3)'];
        return view('dosen::create', compact('strata'));
    }

    public function store(Store $request)
    {
        $request->validated();

        $dosen = Dosen::create($request->only(['nama', 'nip', 'gelar']));

        $request->merge(['dosen_id' => $dosen->id]);

        RiwayatPendidikan::create($request->except('nama', 'nip', 'gelar'));

        return redirect()->back()->withSuccess('Dosen saved');
    }

    public function show(Dosen $dosen)
    {
        $riwayat_pendidikan = RiwayatPendidikan::where('dosen_id', $dosen->id)->first();

        return view('dosen::show', compact('dosen', 'riwayat_pendidikan'));
    }

    public function edit(Dosen $dosen)
    {
        $riwayat_pendidikan = RiwayatPendidikan::where('dosen_id', $dosen->id)->first();

        $strata = ['Sarjana (S1)' => 'Sarjana (S1)', 'Magister (S2)' => 'Magister (S2)', 'Doktor (S3)' => 'Doktor (S3)'];
        return view('dosen::edit', compact('dosen', 'riwayat_pendidikan', 'strata'));
    }

    public function update(Update $request, Dosen $dosen)
    {
        $request->validated();

        $dosen->update($request->only(['nama', 'nip', 'gelar']));

        $id_rwyt_pend = RiwayatPendidikan::where('dosen_id', $dosen->id)->value('id');

        $request->merge(['dosen_id' => $id_rwyt_pend]);

        $inputRiwayat = $request->except('nama', 'nip', 'gelar');

        RiwayatPendidikan::find($id_rwyt_pend)->update($inputRiwayat);

        return redirect()->back()->withSuccess('Dosen saved');
    }

    public function destroy(Dosen $dosen)
    {
        RiwayatPendidikan::find(RiwayatPendidikan::where('dosen_id', $dosen->id)->value('id'))->delete();

        $dosen->delete();

        return redirect()->back()->withSuccess('Dosen deleted');
    }
}
