<?php

namespace Modules\Dosen\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Matakuliah\Models\Matakuliah;

class Dosen extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'dosen';

    protected $guarded = [];

    protected $searchableColumns = ["nama", "nip", "gelar",];

    public function matakuliah()
    {
        return $this->belongsToMany(Matakuliah::class);
    }
}
