@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Matakuliah">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::matakuliah.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
