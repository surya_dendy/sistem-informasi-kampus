@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::matakuliah.index') }}"></x-backlink>

    <x-panel title="Detil Matakuliah">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $matakuliah->id }}</td></tr>
        <tr><td>Nama Matkul</td><td>{{ $matakuliah->nama_matkul }}</td></tr>
        <tr><td>Jum Sks</td><td>{{ $matakuliah->jum_sks }}</td></tr>
        <tr><td>Created At</td><td>{{ $matakuliah->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $matakuliah->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
