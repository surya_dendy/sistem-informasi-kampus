@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::matakuliah.index') }}"></x-backlink>

    <x-panel title="Tambah Matakuliah">
        {!! form()->post(route('modules::matakuliah.store'))->horizontal()->multipart() !!}
        @include('matakuliah::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
