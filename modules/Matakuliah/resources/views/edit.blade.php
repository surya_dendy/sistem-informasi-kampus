@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::matakuliah.index') }}"></x-backlink>

    <x-panel title="Edit Matakuliah">
        {!! form()->bind($matakuliah)->put(route('modules::matakuliah.update', $matakuliah->getKey()))->horizontal()->multipart() !!}
        @include('matakuliah::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
