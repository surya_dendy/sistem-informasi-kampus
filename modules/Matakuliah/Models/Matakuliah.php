<?php

namespace Modules\Matakuliah\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Dosen\Models\Dosen;
use Modules\Mahasiswa\Models\Mahasiswa;

class Matakuliah extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'matakuliah';

    protected $guarded = [];

    protected $searchableColumns = ["nama_matkul", "jum_sks",];

    public function mahasiswa()
    {
        return $this->belongsToMany(Mahasiswa::class);
    }

    public function dosen()
    {
        return $this->belongsToMany(Dosen::class);
    }
}
