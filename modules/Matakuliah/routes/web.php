<?php

use Modules\Matakuliah\Controllers\MatakuliahController;

$router->group(
    [
        'prefix' => config('modules.matakuliah.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.matakuliah.route.middleware'),
    ],
    function ($router) {
        $router->resource('matakuliah', MatakuliahController::class);
    }
);
