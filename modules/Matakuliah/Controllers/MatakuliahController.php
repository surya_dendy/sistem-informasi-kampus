<?php

namespace Modules\Matakuliah\Controllers;

use Illuminate\Routing\Controller;
use Modules\Matakuliah\Requests\Store;
use Modules\Matakuliah\Requests\Update;
use Modules\Matakuliah\Models\Matakuliah;
use Modules\Matakuliah\Tables\MatakuliahTableView;

class MatakuliahController extends Controller
{
    public function index()
    {
        return MatakuliahTableView::make()->view('matakuliah::index');
    }

    public function create()
    {
        return view('matakuliah::create');
    }

    public function store(Store $request)
    {
        Matakuliah::create($request->validated());

        return redirect()->back()->withSuccess('Matakuliah saved');
    }

    public function show(Matakuliah $matakuliah)
    {
        return view('matakuliah::show', compact('matakuliah'));
    }

    public function edit(Matakuliah $matakuliah)
    {
        return view('matakuliah::edit', compact('matakuliah'));
    }

    public function update(Update $request, Matakuliah $matakuliah)
    {
        $matakuliah->update($request->validated());

        return redirect()->back()->withSuccess('Matakuliah saved');
    }

    public function destroy(Matakuliah $matakuliah)
    {
        $matakuliah->delete();

        return redirect()->back()->withSuccess('Matakuliah deleted');
    }
}
