<?php

namespace Modules\Matakuliah\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Matakuliah\Models\Matakuliah;

class MatakuliahTableView extends TableView
{
    public function source()
    {
        return Matakuliah::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nama_matkul')->sortable(),
            Text::make('jum_sks')->sortable(),
            RestfulButton::make('modules::matakuliah'),
        ];
    }
}
