<?php

use App\Http\Controllers\DaftarMatkul;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\DosenMatkul;
use App\Http\Controllers\Home;

Route::get('/', Home::class)->name('home');
Route::get('/dashboard', Dashboard::class)->name('dashboard')->middleware('auth');

Route::resource('daftar-matkul', DaftarMatkul::class);
Route::resource('dosen-matkul', DosenMatkul::class);
