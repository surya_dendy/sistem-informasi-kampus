@extends('laravolt::layouts.app')

@section('content')

<x-backlink url="{{ route('daftar-matkul.index') }}"></x-backlink>

<x-panel title="Edit Daftar Kuliah">
    {!! form()->bind($data)->put(route('daftar-matkul.update',$data->id))->horizontal()->multipart() !!}
    {!! form()->text('nama')->readonly()->label('Nama Mahasiswa') !!}
    {!! form()->selectMultiple('matakuliah_id[]',$matkul,$matkuls)->placeholder('Masukkan Mata
    Kuliah')->label('Nama Mata
    Kuliah')!!}
    {!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('daftar-matkul.index'))
    ]) !!}
    {!! form()->close() !!}
</x-panel>

@stop