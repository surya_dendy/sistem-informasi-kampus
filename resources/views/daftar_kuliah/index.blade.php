@extends('laravolt::layouts.app')

@section('content')


<x-titlebar title="Daftar Kuliah">
    <x-item>
        <x-link label="Tambah " icon="plus" url="{{ route('daftar-matkul.create') }}"></x-link>
    </x-item>
</x-titlebar>

{!! Suitable::source($data)
->columns([
['header'=> 'No', 'field'=> 'id'],
['header' => 'Nama Mahasiswa', 'field' => 'nama'],
['header' => 'Mata Kuliah', 'raw' => function ($item){
$matkuls = array();
foreach ($item->matakuliah as $daf_kuliah) {
$matkuls[] = $daf_kuliah;
}
foreach ($matkuls as $item) {
echo "<li>$item->nama_matkul ($item->jum_sks sks)</li>";
}

}],
['header' => 'Total SKS', 'raw' => function ($item){
$totalSks = 0;
foreach ($item->matakuliah as $sks) {
$totalSks+= (int)$sks->jum_sks;
}
return $totalSks;
}],
['header'=>'Aksi','raw'=> function ($item){
return "<a href=".route('daftar-matkul.edit',$item->id)."><i class='edit icon'></i></a>";
}],
])
->render() !!}
@stop