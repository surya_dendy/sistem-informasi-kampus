@extends('laravolt::layouts.app')

@section('content')

<x-backlink url="{{ route('daftar-matkul.index') }}"></x-backlink>

<x-panel title="Tambah">
    {!! form()->post(route('daftar-matkul.store'))->horizontal()->multipart() !!}
    {!! form()->dropdown('mahasiswa_id',$mahasiswa)->placeholder('Pilih Nama Mahasiswa')->label('Nama Mahasiswa') !!}
    {!! form()->selectMultiple('matakuliah_id[]',$matkul)->placeholder('Masukkan Nama Mata Kuliah')->label('Nama Mata
    Kuliah')!!}
    {!! form()->action([
    form()->submit('Simpan Data matkul'),
    form()->link('Batal', route('daftar-matkul.index'))
    ]) !!}
    {!! form()->close() !!}
</x-panel>

@stop