@extends('laravolt::layouts.app')

@section('content')

<x-backlink url="{{ route('dosen-matkul.index') }}"></x-backlink>

<x-panel title="Edit Daftar Kuliah">
    {!! form()->bind($data)->put(route('dosen-matkul.update',$data->id))->horizontal()->multipart() !!}
    {!! form()->text('nama')->readonly()->label('Nama Dosen') !!}
    {!! form()->selectMultiple('matakuliah_id[]',$matkul,$daf_matkul)->placeholder('Masukkan Mata
    Kuliah')->label('Nama Mata
    Kuliah')!!}
    {!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('dosen-matkul.index'))
    ]) !!}
    {!! form()->close() !!}
</x-panel>

@stop