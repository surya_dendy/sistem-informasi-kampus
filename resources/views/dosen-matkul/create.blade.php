@extends('laravolt::layouts.app')

@section('content')

<x-backlink url="{{ route('dosen-matkul.index') }}"></x-backlink>

<x-panel title="Tambah">
    {!! form()->post(route('dosen-matkul.store'))->horizontal()->multipart() !!}
    {!! form()->dropdown('dosen_id',$dosen)->placeholder('Pilih Nama Dosen')->label('Nama Dosen') !!}
    {!! form()->selectMultiple('matakuliah_id[]',$matkul)->placeholder('Pilih Mata Kuliah')->label('Nama Mata
    Kuliah')!!}
    {!! form()->action([
    form()->submit('Simpan Data'),
    form()->link('Batal', route('dosen-matkul.index'))
    ]) !!}
    {!! form()->close() !!}
</x-panel>

@stop