<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Mahasiswa\Models\Mahasiswa;
use Modules\Matakuliah\Models\Matakuliah;

class DaftarMatkul extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $data = Mahasiswa::with('matakuliah')->get();


        return view('daftar_kuliah.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $mahasiswa = Mahasiswa::all()->pluck('nama', 'id');
        $matkul = Matakuliah::all()->pluck('nama_matkul', 'id');

        return view('daftar_kuliah.create', compact('mahasiswa', 'matkul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $mahasiswa = Mahasiswa::find($request->mahasiswa_id);

        $matkul = $request->matakuliah_id;

        $total_sks = 0;
        foreach ($matkul as $item) {

            $sks_matkul = Matakuliah::where('id', (int)$item)->select('jum_sks')->first();
            $total_sks += $sks_matkul->jum_sks;
        }


        if ($total_sks > 24) {
            return redirect()->back()->withErrors('Mata Kuliah melebihi 24 SKS yaitu ' . $total_sks . ' sks');
        } else {

            $mahasiswa->matakuliah()->attach($matkul);
            return redirect()->back()->withSuccess('Mata Kuliah diambil dengan total sks ' . $total_sks . ' sks');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Mahasiswa::find($id);
        $matkul = Matakuliah::all()->pluck('nama_matkul', 'id');

        $pivot = DB::table('mahasiswa_matakuliah')->where('mahasiswa_id', $id)->get();

        $matkuls = array();
        foreach ($pivot as $item) {
            $id_matkul = MataKuliah::where('id', $item->matakuliah_id)->first();
            array_push($matkuls, $id_matkul->nama_matkul);
        }

        return view('daftar_kuliah.edit', compact('data', 'matkul', 'matkuls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $matkul_tambahan_string = array();
        $matkul_lama_string =  array();
        $matkul_lama = array();


        //request dari form
        $request_matkul = $request->matakuliah_id;

        //memisahkan matkul
        foreach ($request_matkul as $item) {
            (int)$item;
            if ($item != 0) {
                array_push($matkul_tambahan_string, (int)$item);
            } else {
                array_push($matkul_lama_string, $item);
            }
        }

        //dapetikan id dari string matkul
        foreach ($matkul_lama_string as $item) {
            $matkul_str = MataKuliah::where('nama_matkul', $item)->select('id')->first();
            array_push($matkul_lama, $matkul_str->id);
        }

        //merge matkul lama dan terbaru
        $matkul_update = array_merge($matkul_tambahan_string, $matkul_lama);

        $total_sks = 0;
        foreach ($matkul_update as $item) {

            $sks_matkul = Matakuliah::where('id', $item)->select('jum_sks')->first();
            $total_sks += $sks_matkul->jum_sks;
        }


        // simpan ke user
        $mahasiswa = Mahasiswa::find($id);

        if ($total_sks > 24) {
            return redirect()->back()->withErrors('Mata Kuliah melebihi 24 SKS yaitu ' . $total_sks . ' sks');
        } else {
            $mahasiswa->matakuliah()->sync($matkul_update);
            return redirect()->back()->withSuccess('Mata Kuliah diupdate dengan total sks ' . $total_sks . ' sks');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //belum selesai



        $pivot = DB::table('mahasiswa_matakuliah')->where('mahasiswa_id', $id)->get();
        dd($pivot);
        $matkul = array();
        foreach ($pivot as $item) {
            $id_matkul = MataKuliah::where('id', $item->matakuliah_id)->first();
            array_push($matkul, $id_matkul->id);
        }

        $mahasiswa = Mahasiswa::find($id);

        $mahasiswa->matakuliah()->detach($matkul);

        return redirect()->back()->withSuccess('Mata Kuliah pada mahasiswa ' . $pivot->nama . ' berhasil dihapus');
    }
}
