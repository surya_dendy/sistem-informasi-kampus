<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Dosen\Models\Dosen;
use Modules\Matakuliah\Models\Matakuliah;

class DosenMatkul extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Dosen::with('matakuliah')->get();


        return view('dosen-matkul.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $dosen = Dosen::all()->pluck('nama', 'id');
        $matkul = Matakuliah::all()->pluck('nama_matkul', 'id');

        return view('dosen-matkul.create', compact('dosen', 'matkul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $dosen = Dosen::find($request->dosen_id);

        $matkul = $request->matakuliah_id;

        $dosen->matakuliah()->attach($matkul);
        return redirect()->back()->withSuccess(count($matkul) . ' Mata Kuliah berhasil diambil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Dosen::find($id);

        $matkul = Matakuliah::all()->pluck('nama_matkul', 'id');

        $pivot = DB::table('mahasiswa_matakuliah')->where('mahasiswa_id', $id)->get();

        $daf_matkul = array();
        foreach ($pivot as $item) {
            $id_matkul = MataKuliah::where('id', $item->matakuliah_id)->first();
            array_push($daf_matkul, $id_matkul->nama_matkul);
        }
        // dd($matkuls);

        return view('dosen-matkul.edit', compact('data', 'matkul', 'daf_matkul'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $matkul_tambahan_string = array();
        $matkul_lama_string =  array();
        $matkul_lama = array();

        //request dari form
        $request_matkul = $request->matakuliah_id;

        //memisahkan matkul
        foreach ($request_matkul as $item) {
            (int)$item;
            if ($item != 0) {
                array_push($matkul_tambahan_string, (int)$item);
            } else {
                array_push($matkul_lama_string, $item);
            }
        }

        //dapetikan id dari string matkul
        foreach ($matkul_lama_string as $item) {
            $matkul_str = MataKuliah::where('nama_matkul', $item)->select('id')->first();
            array_push($matkul_lama, $matkul_str->id);
        }

        //merge matkul lama dan terbaru
        $matkul_update = array_merge($matkul_tambahan_string, $matkul_lama);
        // dd($matkul_update);
        // dd($matkul_update);
        // simpan ke user
        $dosen = Dosen::find($id);

        $dosen->matakuliah()->sync($matkul_update);
        return redirect()->back()->withSuccess('Mata Kuliah berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
