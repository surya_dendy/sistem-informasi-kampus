<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan extends Model
{
    use HasFactory;

    protected $table = 'riwayat_pendidikan';

    protected $fillable = ['strata', 'jurusan', 'sekolah', 'tahun_mulai', 'tahun_selesai', 'dosen_id'];

    protected $dates = ['tahun_mulai', 'tahun_selesai'];
}
